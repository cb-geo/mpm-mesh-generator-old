#include "boundary.h"
#include <math.h>

#include <iostream>
using namespace std;

//---------------------------------------------------------------------

void boundary_data (int *nb, int nbound, int *loctn, int *bn_mesh, int **nodes){
	
    for (int i=0; i<nbound; i++){				//obtain no. of nodes per boundary
	if (loctn[i]==1) nb[i] = nodes[bn_mesh[i]][0]; 			//boundary location 1-bottom
	else if (loctn[i]==2) nb[i] = nodes[bn_mesh[i]][0]; //boundary location 2-top
	else if (loctn[i]==3) nb[i] = nodes[bn_mesh[i]][1]; //boundary location 3-LHS
	else if (loctn[i]==4) nb[i] = nodes[bn_mesh[i]][1]; //boundary location 4-RHS
    }
		
}
//---------------------------------------------------------------------

void boundary_nodes (int **bnode, int *nb, int nbound, int *bn_mesh, int *loctn, int **nodes, int nmesh){
    //obtain boundary node numbers
    int cum_nodes[nmesh+1];
	
    for (int i=0; i<=nmesh; i++)
	cum_nodes[i] = 0;
		
    for (int i=0; i<nmesh; i++)
	cum_nodes[i+1] = cum_nodes[i] + nodes[i][0]*nodes[i][1];
		
    for (int i=0; i<nbound; i++){
	for (int j=0; j<nb[i]; j++){
	    if (loctn[i]==1) bnode[i][j] = cum_nodes[bn_mesh[i]] + j;
	    else if (loctn[i]==2) bnode[i][j] = cum_nodes[bn_mesh[i]] + nodes[bn_mesh[i]][0]*(nodes[bn_mesh[i]][1]-1) + j;
	    else if (loctn[i]==3) bnode[i][j] = cum_nodes[bn_mesh[i]] + nodes[bn_mesh[i]][0]*j;
	    else if (loctn[i]==4) bnode[i][j] = cum_nodes[bn_mesh[i]] + nodes[bn_mesh[i]][0]*(j+1) -1;				
	}	
    }

}
//---------------------------------------------------------------------

void boundary_directions (int nbound, int *bn_type, int *loctn, int *direction, int* signNormDirFriction){
    // boundary location =>  1-bot, 2-top, 3-LHS, 4-RHS 

    for (int i=0; i<nbound; i++){
	if( bn_type[ i ] == 0 ){
	    if (loctn[i]==1) direction[i]      = 1;
	    else if (loctn[i]==2) direction[i] = 1;
	    else if (loctn[i]==3) direction[i] = 0;
	    else if (loctn[i]==4) direction[i] = 0;
	    signNormDirFriction[ i ] = 9999999; //because not applicable
	}else if( bn_type[ i ] == 1 ){
	    if (loctn[i]==1){
		direction[ i ]           = 1;
		signNormDirFriction[ i ] = -1;
	    }
	    else if (loctn[i]==2){
		direction[i]             = 1;
		signNormDirFriction[ i ] = 1;
	    }
	    else if (loctn[i]==3){
		direction[i]             = 0;
		signNormDirFriction[ i ] = -1;
	    }
	    else if (loctn[i]==4){
		direction[i]             = 0;
		signNormDirFriction[ i ] = 1;
	    }
	}
    }

    

}


//---------------------------------------------------------------------

void boundary_velocity (double **v_n, int **bnode, int *loctn, int *mesh_type, int *bn_mesh, double **xmin, double **xmax, int *nb, int nbound){
	
    double theta;
	
    for (int i=0; i<nbound; i++){
	if ((loctn[i]==1)||(loctn[i]==2)){		//bottom or top boundary
	    if (mesh_type[bn_mesh[i]]==1) 		//rectangular mesh
		for (int j=0; j<nb[i]; j++)	v_n[bnode[i][j]][1]=0.0;
	    else if (mesh_type[bn_mesh[i]]==2){	//slope mesh
		theta = atan((xmax[bn_mesh[i]][1]-xmin[bn_mesh[i]][1])/(xmax[bn_mesh[i]][0]-xmin[bn_mesh[i]][0]));
		for (int j=0; j<nb[i]; j++){
		    v_n[bnode[i][j]][0] = (v_n[bnode[i][j]][0]*cos(theta)+v_n[bnode[i][j]][1]*sin(theta))*cos(theta);
		    v_n[bnode[i][j]][1] = (v_n[bnode[i][j]][0]*cos(theta)+v_n[bnode[i][j]][1]*sin(theta))*sin(theta);					
		}
	    }
	}else if ((loctn[i]==3)||(loctn[i]==4))	//LHS or RHS boundary
	    for (int j=0; j<nb[i]; j++)	v_n[bnode[i][j]][0]=0.0;	//for both rect and slope meshes	
    }

}
//---------------------------------------------------------------------

int sign(double var){
    int a;
    if (var>0.0) a=1;
    if (var<0.0) a=-1;
    if (var==0.0) a=0;
    return a;
}
//---------------------------------------------------------------------

void boundary_acc (double **a_n, double **v_n, double *miu, int **bnode, int *mesh_type, int *bn_mesh, int *loctn, int *nb, double **xmin, double **xmax, double dt, int nbound){

    double theta;
    double at, an, vt;
	
    for (int i=0; i<nbound; i++){
	if ((loctn[i]==1)||(loctn[i]==2)){		//bottom or top boundary
	    if (mesh_type[bn_mesh[i]]==1){ 			//rectangular mesh
		for (int j=0; j<nb[i]; j++)	{
//-----					
		    if (a_n[bnode[i][j]][1]<0.0){
			if (v_n[bnode[i][j]][0] != 0.0){
			    if (fabs(v_n[bnode[i][j]][0])<=dt*miu[i]*fabs(a_n[bnode[i][j]][1]))
				a_n[bnode[i][j]][0] = -v_n[bnode[i][j]][0]/dt;
			    else
				a_n[bnode[i][j]][0] -= sign(v_n[bnode[i][j]][0])*miu[i]*fabs(a_n[bnode[i][j]][1]);
							
			}else{
			    if (fabs(a_n[bnode[i][j]][0])<=miu[i]*fabs(a_n[bnode[i][j]][1])){
				a_n[bnode[i][j]][0] = -v_n[bnode[i][j]][0]/dt; // =0 should be
			    }else	
				a_n[bnode[i][j]][0] -= sign(a_n[bnode[i][j]][0])*miu[i]*fabs(a_n[bnode[i][j]][1]);
			}
			/*if (v_n[bnode[i][j]][0] != 0.0)   //keita method _only for slope
			  a_n[bnode[i][j]][0] -= sign(v_n[bnode[i][j]][0])*miu[i]*fabs(a_n[bnode[i][j]][1]);
			  else a_n[bnode[i][j]][0] = 0.0;*/	
		    }				
		    a_n[bnode[i][j]][1] = 0.0;
//-----					
					
/*keitab- if (a_n[bnode[i][j]][1]<0.0){
  if (v_n[bnode[i][j]][0] != 0.0)
  a_n[bnode[i][j]][0] -= sign(v_n[bnode[i][j]][0])*miu[i]*fabs(a_n[bnode[i][j]][1]);
  else a_n[bnode[i][j]][0] = 0.0;	
  }
  a_n[bnode[i][j]][1] = 0.0;														-keitab*/						
		}
	    }
	    else if (mesh_type[bn_mesh[i]]==2){	//slope mesh
		theta = atan((xmax[bn_mesh[i]][1]-xmin[bn_mesh[i]][1])/(xmax[bn_mesh[i]][0]-xmin[bn_mesh[i]][0]));
		for (int j=0; j<nb[i]; j++){
		    at = a_n[bnode[i][j]][0]*cos(theta) + a_n[bnode[i][j]][1]*sin(theta);
		    an = a_n[bnode[i][j]][1]*cos(theta) - a_n[bnode[i][j]][0]*sin(theta);
		    vt = v_n[bnode[i][j]][0]*cos(theta) + v_n[bnode[i][j]][1]*sin(theta);
//  SAMILA boundary start --- 
/*		    if (loctn[i]==1){
		    if (an<0.0){
		    if (vt != 0.0){ //(fabs(vt)>1e-8){
		    //if (bnode[i][j]==11421) cout<<miu[i]*fabs(an)*dt<<" , "<<vt<<"\n";
		    if (fabs(vt)<=miu[i]*fabs(an)*dt)
		    at = -vt/dt;
		    else
		    at -= sign(vt)*miu[i]*fabs(an);
		    }else{
		    //cout<<" hi "<<vt<<"\n";
		    if (fabs(at) <= miu[i]*fabs(an))
		    at = -vt/dt;
		    else
		    at -= sign(at)*miu[i]*fabs(an);							
		    } 	
		    }
		    an = 0.0;											
		    }else if (loctn[i]==2){
		    if (an>0.0){
		    if (fabs(at) <= miu[i]*fabs(an))
		    at = -vt/dt;
		    else
		    at -= sign(vt)*miu[i]*fabs(an);	
		    }
		    an = 0.0;							
		    }*/
//  SAMILA boundary end --- 
//  KEITA boundary start --- 
		    if (loctn[i]==1){
			if (an<0.0){
			    if (vt != 0.0) //keita method _only for slope   //keita start
				at -= sign(vt)*miu[i]*fabs(an);
			    else at = 0.0;                                //keita end	 
			}
			an = 0.0;											
		    }else if (loctn[i]==2){
			if (an>0.0){
			    if (vt != 0.0) //keita method _only for slope   //keita start
				at -= sign(vt)*miu[i]*fabs(an);
			    else at = 0.0;	                            //keita end					
			}
			an = 0.0;							
		    }
//  KEITA boundary end ---
		    
		    /*}else if (loctn[i]==2){   //some other boundary
		      if (an>0.0){
		      if (vt != 0.0) //((vt<-1e-30)&&(vt>1e-30))
		      at -= sign(vt)*miu[i]*fabs(an);
		      else at = 0.0;	
		      }
		      an = 0.0;							
		      }*/
					
		    a_n[bnode[i][j]][0] = at*cos(theta) - an*sin(theta);
		    a_n[bnode[i][j]][1] = at*sin(theta) + an*cos(theta);					
		}
	    }
	}else if ((loctn[i]==3)||(loctn[i]==4))	//LHS or RHS boundary
	    for (int j=0; j<nb[i]; j++)	a_n[bnode[i][j]][0]=0.0;	//for both rect and slope meshes	
    }	

}
//---------------------------------------------------------------------





