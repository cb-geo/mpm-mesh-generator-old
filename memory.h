#ifndef	MEMORY_H
#define	MEMORY_H


double *allocate_1d_double_array (int);

double **allocate_2d_double_array (int,int);

double ***allocate_3d_double_array (int ,int ,int);

int *allocate_1d_int_array (int);

int **allocate_2d_int_array (int,int);

int **allocate_2d_int_variable_array (int,int *);

int ***allocate_3d_int_array (int ,int ,int);

void free_1d_double_array (double *);

void free_2d_double_array (double **, int);

void free_3d_double_array (double ***, int ,int);

void free_1d_int_array (int *);

void free_2d_int_array (int **, int);

void free_3d_int_array (int ***, int ,int);


#endif
