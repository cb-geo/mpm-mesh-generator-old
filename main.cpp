#include "memory.h" 
#include "mesh.h" 
#include "boundary.h"
// #include "particlePlot.h"

#include <time.h>

#include <math.h> 
#include <string.h> 
#include <iostream> 
#include <fstream> 
#include <cstdlib>
#include <sys/stat.h>  // to make directory
#include <sys/types.h> // to make directory
 
using namespace std; 
 
int roundval(double v){ 
    int b; 
    b = v < 0.0 ? ceil (v-0.5) : floor (v+0.5); 
    return b; 
} 

int main() { 
	 
    int nmesh, nbound; 
    double **xmin, **xmax, start_ymin, start_ymax; 
    double density, youngs, poisson ,phi, psi, coh, sigt, soilPSpacing; 
    int nmesh_y, *nmesh_x, *mesh_id, *mesh_type; 
    int *bn_id, *bn_mesh, *loctn, *bn_type, *direction, *signNormDirFriction; 
    double porosity, miu_all; 
    int steps, write_step, model, gravity_flag; 
    double dt;
    // string  inputFile, meshFile, submeshFile, meshConstraintsFile, materialFile;
    char inputFile[100], meshFile[100], submeshFile[100], meshConstraintsFile[100], materialFile[100];
    char inputFileCreator[100], fileName[20], outPath[100], resultsFolder[100];
    int status;

    //-------- Input Files data  __________
    // inputFileCreator     = "inputFileCreator/inputFileCreator.txt";
    cout << "File name of input file creator (*.txt) : " ;
    char inPath[] = "inFiles/";
    cin >> fileName ;

    strcpy (inputFileCreator, inPath );
    strcat (inputFileCreator, fileName );
    strcat (inputFileCreator, ".txt" );

    strcpy (outPath, "outFiles/" );
    strcat (outPath, fileName );
    status = mkdir( outPath, 0777 ); //creates a directory named with fileName

    strcpy ( resultsFolder, outPath );
    strcat (resultsFolder, "/results" );
    status = mkdir( resultsFolder, 0777 ); //creates a directory named with fileName
    
    strcat (outPath, "/inputFiles" );
    status = mkdir( outPath, 0777 ); //creates a directory named with fileName
    strcat (outPath, "/" );
    
    cout << "Input files were created at: " << outPath  <<"\n";
    
    strcpy (inputFile, outPath );
    strcat (inputFile, "input.dat" );
    strcpy (meshFile, outPath );
    strcat (meshFile, "mesh.smf" );
    strcpy (submeshFile, outPath );
    strcat (submeshFile, "submesh.dat" );
    strcpy (meshConstraintsFile, outPath );
    strcat (meshConstraintsFile, "mesh.constraints" );
    strcpy (materialFile, outPath );
    strcat (materialFile, "material.dat" );
    
    //-------- Input files files __________
    // inputFile            = "outFiles/inputFiles/input.dat";
    // meshFile             = "outFiles/inputFiles/mesh.dat";
    // submeshFile          = "outFiles/inputFiles/submesh.dat";
    // meshConstraintsFile  = "outFiles/inputFiles/mesh.constraints";
    // materialFile         = "outFiles/inputFiles/material.dat";
 
    // ------ end of file definitions ----------
    
    // ifstream file1 (inputFileCreator.c_str()); // when inputFileCreator is a string
    ifstream file1 (inputFileCreator); //when inputFileCreator is a character array
 
    file1 >> dt >> steps >> write_step; 
    file1 >> gravity_flag >> miu_all; 
    file1 >> nmesh >> nbound; 

    mesh_id = allocate_1d_int_array(nmesh); 
    mesh_type = allocate_1d_int_array(nmesh); 
    xmin = allocate_2d_double_array (nmesh,2); 
    xmax = allocate_2d_double_array (nmesh,2); 
    nmesh_x = allocate_1d_int_array(nmesh); 
    bn_id = allocate_1d_int_array(nbound); 
    bn_mesh = allocate_1d_int_array(nbound); 
    loctn = allocate_1d_int_array(nbound); 
    bn_type = allocate_1d_int_array(nbound); 
    direction = allocate_1d_int_array(nbound); 
    signNormDirFriction = allocate_1d_int_array(nbound); 
	 
    file1 >> model >> soilPSpacing;	 
    if (model == 1) file1 >> density >> youngs >> poisson >> phi >> porosity; 
    if (model == 2) file1 >> density >> youngs >> poisson >> phi >> psi >> coh >> sigt >> porosity; 
    file1 >> start_ymin >> start_ymax >> nmesh_y; 
    for(int i = 0; i < nmesh; i++)  
	file1 >> mesh_id[i] >> mesh_type[i] >> xmin[i][0] >> xmax[i][0] >> nmesh_x[i] >> xmin[i][1] >> xmax[i][1];  
    for(int i = 0; i < nbound; i++) 
	file1 >> bn_id[i] >> bn_mesh[i] >> loctn[i] >> bn_type[i]; 
 
    file1.close(); 

    ofstream inputF( inputFile );
    ofstream meshF( meshFile );
    ofstream subMeshF( submeshFile );
    ofstream meshConsF( meshConstraintsFile );
    ofstream materialF( materialFile );
    
//---------------------------------------------------   
 
    int **nodes, elements_total, *elementsSubMesh;
    int nn, *nb; 
    double **intvl; 
    double **x_n;
    int **node, ***element, **elnode, **bnode, nodesX; 
   
    nodes = allocate_2d_int_array (nmesh, 2);	 

    intvl = allocate_2d_double_array (nmesh, 2); 

    nn = mesh_data (nodes, intvl, nmesh, nmesh_y, nmesh_x, xmin, xmax, start_ymin, start_ymax); 

    nodesX = mesh_sum_nodesx (nodes, nmesh); 

    nb = allocate_1d_int_array(nbound);   

    boundary_data (nb, nbound, loctn, bn_mesh, nodes); 

    elementsSubMesh = allocate_1d_int_array(4);   
	 
    x_n = allocate_2d_double_array (nn, 2); 

    node = allocate_2d_int_array (nodesX, nodes[0][1]); 

    element = allocate_3d_int_array (nmesh, nodesX-1, nodes[0][1]-1); 

    elements_total = element_data( nodes, nmesh, nmesh_y, nmesh_x, elementsSubMesh);

    elnode = allocate_2d_int_array (elements_total, 4); 

    bnode = allocate_2d_int_variable_array (nbound,nb);	 

    mesh_nodes (x_n, node, nmesh, mesh_type, nodes, intvl, xmin, xmax);
    
    boundary_nodes (bnode, nb, nbound, bn_mesh, loctn, nodes, nmesh);

    boundary_directions (nbound, bn_type, loctn, direction, signNormDirFriction);

    elementNodeConnecttivity(node, element, elnode, xmin, xmax, nodes, mesh_type, nmesh, nmesh_y, nmesh_x, elements_total);
    // cout<<"ok upto here"<<"\n";
    
//--------------------------------------------------- input.dat
    inputF << "#-- input output file names --"  << "\n";
    inputF << "inputMeshFileName              " << fileName << "/inputFiles/mesh.smf" << "\n";
    inputF << "inputSubMeshFileName           " << fileName << "/inputFiles/submesh.dat" << "\n";
    inputF << "constraintsFileName   	       " << fileName << "/inputFiles/mesh.constraints" << "\n";
    inputF << "inputSoilParticleFileName      " << fileName << "/inputFiles/soilParticles.smf" << "\n";
    inputF << "initStressSoilPFileName        " << fileName << "/inputFiles/initStressSoilP.dat" << "\n";
    inputF << "materialFileName               " << fileName << "/inputFiles/material.dat" << "\n";
    inputF << "outputFileName                 " << fileName << "/results/results1.ile" << "\n"<<"\n";
    inputF << "#-- other parameters --"  << "\n";
    inputF << "gravityFlag                    " << gravity_flag << "\n";
    inputF << "boundaryFrictionMiu            " << miu_all << "\n";
    inputF << "soilParticleSpacing            " << soilPSpacing << "\n";
    inputF << "timeInterval                   " << dt << "\n";
    inputF << "numberOfSteps                  " << steps << "\n";
    inputF << "numberOfSubStepsOS             " << write_step ;

//--------------------------------------------------- mesh.dat  	 
    meshF << "! elementShape quadrilateral "<< "\n"; 
    meshF << "! elementNumPoints 4 "<< "\n"; 
    meshF << nn << " " << elements_total << "\n"; 
    for( int i = 0; i < nn; i++ ){	 
    	meshF << x_n[i][0] << " " <<x_n[i][1] << " " << "0" << "\n"; 
    }
    for( int i = 0; i < elements_total; i++ ){	 
    	meshF << elnode[i][0] << " " <<elnode[i][1] << " " << elnode[i][2] << " " <<elnode[i][3] << "\n"; 
    }

//--------------------------------------------------- submesh.dat

    subMeshF << nmesh  << "\n"; 
    for( int m = 0; m < nmesh; m++ ){
	subMeshF<< element[ m ][ 0 ][ 0 ] << " " << element[ m ][ nmesh_x[ m ]-1 ][ 0 ]
		 << " " << element[ m ][ 0 ][ nmesh_y-1 ] <<" "
		<< element[ m ][ nmesh_x[ m ]-1 ][ nmesh_y-1 ] <<" "<<mesh_type[ m ]<< "\n";
    }    

//--------------------------------------------------- mesh.constraints

    int numGeneralBoundaries = 0;
    int numFrictionBoundaries = 0;
    for ( int i = 0; i < nbound; i++ ){
	if( bn_type[ i ] == 0 ) numGeneralBoundaries += nb[ i ];
	else if( bn_type[ i ] == 1 ) numFrictionBoundaries += nb[ i ];
	// cout << numGeneralBoundaries << " , "<<numFrictionBoundaries<<"\n";
    }

    meshConsF << numGeneralBoundaries <<" "<< numFrictionBoundaries<<"\n";
    for ( int i = 0; i < nbound; i++ ){
	for ( int j = 0; j < nb[ i ]; j++ ){
	    if( bn_type[ i ] == 0 )
		meshConsF << bnode[ i ][ j ]<<" "<<direction[ i ]<<"\n";
	}
    }
    for ( int i = 0; i < nbound; i++ ){
	for ( int j = 0; j < nb[ i ]; j++ ){
	    if( bn_type[ i ] == 1 )
		meshConsF << bnode[ i ][ j ]<<" "<<direction[ i ]<<" "<<signNormDirFriction[ i ]<<"\n";
	}
    }
    
//--------------------------------------------------- material.dat

    if( model == 1 ){
	materialF << "IsotropLinElastic {" << "\n";
	materialF << "       youngModulus " << youngs <<"\n";
	materialF << "       poissonRatio " << poisson <<"\n";
	materialF << "       phi " << phi <<"\n";
	materialF << "       porosity " << porosity <<"\n";
	materialF << "       density " << density <<"\n";
	materialF << "}" << "\n";
    }else if( model == 2 ){
	materialF << "MohrCoulomb {" << "\n";
	materialF << "       youngModulus " << youngs <<"\n";
	materialF << "       poissonRatio " << poisson <<"\n";
	materialF << "       phi " << phi <<"\n";
	materialF << "       psi " << psi <<"\n";
	materialF << "       cohesion " << coh <<"\n";
	materialF << "       sigt " << sigt <<"\n";
	materialF << "       porosity " << porosity <<"\n";
	materialF << "       density " << density <<"\n";
	materialF << "}" << "\n";
    }
    
//---------------------------------------------------   	 

    free_2d_double_array(x_n, nn); 
    free_2d_int_array(node, nodesX); 
    free_3d_int_array(element, nmesh, nodesX-1); 
    free_2d_int_array(elnode, elements_total); 
    free_2d_int_array(bnode, nbound); 
    free_1d_int_array(mesh_id); 
    free_1d_int_array(mesh_type); 
    free_2d_double_array(xmin, nmesh);	 
    free_2d_double_array(xmax, nmesh);	 
    free_1d_int_array(nmesh_x); 
    free_1d_int_array(bn_id); 
    free_1d_int_array(bn_mesh); 
    free_1d_int_array(loctn); 
    free_1d_int_array(bn_type); 
	 
    inputF.close(); 
    meshF.close(); 
    subMeshF.close(); 
    meshConsF.close(); 
    materialF.close(); 
   
    return 0; 
}
