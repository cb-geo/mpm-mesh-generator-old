#ifndef	MESH_H
#define	MESH_H

int mesh_data (int **, double **, int, int, int *, double **, double **, double, double);
int element_data (int **, int, int, int *, int *);
void elementNodeConnecttivity(int **, int ***, int **, double **, double **, int **, int *, int, int, int *, int);
int mesh_sum_nodesx (int **, int);
void mesh_nodes (double **, int **, int, int *, int **, double **, double **, double **);



#endif
