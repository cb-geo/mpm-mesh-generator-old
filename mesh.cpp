#include "mesh.h"

#include <iostream>
using namespace std;

//---------------------------------------------------------------------

int mesh_data (int **nodes, double **intvl, int nmesh, int nmesh_y, int *nmesh_x, double **xmin, double **xmax, double start_ymin, double start_ymax){
	
	int nn = 0;
	for(int i=0; i<nmesh; i++){
		if (i==0) nodes[i][0] = nmesh_x[i] + 1;
		else nodes[i][0] = nmesh_x[i];				//no. of nodes in x direction per one mesh
		nodes[i][1] = nmesh_y + 1;						//no. of nodes in y direction per one mesh
		nn += nodes[i][0]*nodes[i][1];				//total no. of nodes for all the meshes
		intvl[i][0] = (xmax[i][0]-xmin[i][0])/nmesh_x[i];	//mesh intvl in x dir per mesh
		intvl[i][1] = (start_ymax-start_ymin)/nmesh_y;		//mesh intvl in y dir per mesh
		
	}
	return nn;
	
}
//---------------------------------------------------------------------
int element_data (int **nodes, int nmesh, int nmesh_y, int *nmesh_x, int *elementsSubMesh ){
	
	int elements_total = 0;
	for(int i=0; i<nmesh; i++){
	    elements_total +=  ( nmesh_x[i] ) * ( nmesh_y );
	}
	return elements_total;
	
}
//---------------------------------------------------------------------

int mesh_sum_nodesx (int **nodes, int nmesh){
	
	int sumx = 0;
	for(int i=0; i<nmesh; i++)
		sumx += nodes[i][0];			//total number of nodes in x direction for all the meshes
	return sumx;
	
}
//---------------------------------------------------------------------

void mesh_nodes (double **x_n, int **node, int nmesh, int *mesh_type, int **nodes, double **intvl, double **xmin, double **xmax){
								//obtain nodal coordinates
    int n = 0;
    double tantheta;
	
    int cum_nodesx[nmesh+1];
	
    for (int i=0; i<=nmesh; i++)
	cum_nodesx[i] = 0;
		
    for (int i=0; i<nmesh; i++)
	cum_nodesx[i+1] = cum_nodesx[i] + nodes[i][0];
		
    for(int i=0; i<nmesh; i++){
	if (mesh_type[i]==1) tantheta = (xmax[i][1]-xmin[i][1])/(xmax[i][0]-xmin[i][0]);
	for(int j=0; j<nodes[i][1]; j++){
	    for (int k=0; k<nodes[i][0]; k++){
		node[cum_nodesx[i]+k][j] = n;
		if (i==0)
		    x_n[n][0] = xmin[i][0] + intvl[i][0]*k;
		else 
		    x_n[n][0] = xmin[i][0] + intvl[i][0]*(k+1);
		if (mesh_type[i]==0) 
		    x_n[n][1] = xmin[i][1] + intvl[i][1]*j;
		else {
		    if ((i==0)&&(k==0)) x_n[n][1] = xmin[i][1] + intvl[i][1]*j;
		    else x_n[n][1] = xmin[i][1] + (x_n[n][0]-xmin[i][0])*tantheta + intvl[i][1]*j;
		}
		n++;
	    }
	}	
    }	
		
}

//---------------------------------------------------------------------

void elementNodeConnecttivity(int **node, int ***element, int **elnode, double **xmin, double **xmax, int **nodes, int *mesh_type, int nmesh, int nmesh_y, int *nmesh_x, int elements_total){

    int lo[elements_total][2], hi[elements_total][2];
    int cum_nodesx[nmesh+1];
    int en=0;
    double tantheta[nmesh];	//Slope of the sloped mesh
		
    for (int i=0; i<=nmesh; i++) cum_nodesx[i] = 0;		
    for (int i=0; i<nmesh; i++) cum_nodesx[i+1] = cum_nodesx[i] + nodes[i][0];
    cum_nodesx[0] = 1;	
	
    for(int i=0; i<nmesh; i++)
	if (mesh_type[i]==1) tantheta[i] = (xmax[i][1]-xmin[i][1])/(xmax[i][0]-xmin[i][0]);

    for( int m = 0; m < nmesh; m++ ){
	// cout<<nodes[ m ][ 1 ]<<" , "<<nodes[ m ][ 0 ]<<"\n";
	for( int i = 0; i < nmesh_y ; i++ ){
	    for ( int j = 0; j < nmesh_x[ m ]; j++ ){
		// cout<<nodes[ m ][ 1 ]<<" , "<<nodes[ m ][ 0 ]<<"\n";
		element[ m ][ j ][ i ] = en;
		lo[i][0] = cum_nodesx[ m ] -1 + j;
		hi[i][0] = lo[i][0] + 1;
		lo[i][1] = i;
		hi[i][1] = lo[i][1] + 1;
		elnode[en][0] = node [lo[i][0]] [lo[i][1]];
		elnode[en][1] = node [hi[i][0]] [lo[i][1]];
		elnode[en][2] = node [hi[i][0]] [hi[i][1]];
		elnode[en][3] = node [lo[i][0]] [hi[i][1]];	
		en += 1;
		
	    }
	}
    }


    // for(int i=0; i<nodes[0][1]-1; i++){
    // 	for(int j=0; j<cum_nodesx[nmesh]-1; j++){
    // 	    lo[i][0] = j;
    // 	    hi[i][0] = lo[i][0] + 1;
    // 	    lo[i][1] = i;
    // 	    hi[i][1] = lo[i][1] + 1;
    // 	    elnode[en][0] = node [lo[i][0]] [lo[i][1]];
    // 	    elnode[en][1] = node [hi[i][0]] [lo[i][1]];
    // 	    elnode[en][2] = node [hi[i][0]] [hi[i][1]];
    // 	    elnode[en][3] = node [lo[i][0]] [hi[i][1]];	
    // 	    en += 1;
		
    // 	}
    // }

}
//---------------------------------------------------------------------
