#ifndef	BOUNDARY_H
#define	BOUNDARY_H

void boundary_data (int *, int, int *, int *, int **);

void boundary_nodes (int **, int *, int, int *, int *, int **, int);

void boundary_directions (int, int *, int *, int *, int*);

void boundary_velocity (double **, int **, int *, int *, int *, double **, double **, int *, int);

int sign(double);

void boundary_acc (double **, double **, double *, int **, int *, int *, int *, int *, double **, double **, double, int);


#endif
