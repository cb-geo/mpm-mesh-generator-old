1e-5 100 10
0 -9.81
346 6 11
0.0025 1600 2 0
2e6 0.3 40 0 0.001 0
0.0	0.15	30
0 1 -0.5 0.2 140 0.0 0.0
1	2	0.2 0.212 3 0.0	0.0008
2	2	0.212 0.224 3 0.0008	0.0032
3	2	0.224 0.236 3 0.0032	0.0073
4	2	0.236 0.248 3 0.0073	0.0132
5	2	0.248 0.64 79 0.0132	0.2396
0	0 1 1 0.5095
1 1 2 1 0.5095
2 2 2 1 0.5095
3 0 1 3 0.0
4 5 1 4 0.0
5 0 1 2 0.0
6 1 1 2 0.0
7 2 1 2 0.0
8 3 2 1 0.5095
9 4 2 1 0.5095
10 5 2 1 0.5095

//1e-5 150000 1000
#--------------------------------------------------------------------#
 line1->   1-dt, 2-Number of time steps, 3-writing steps
 
 line2->   1-gx, 2-gy 
 
 line3->   1-No. of particles, 2-No. of mesh zones, 3-No. of boudary zones 
 
 line4->   1-particle interval, 2-density, 3-model, 4-Jaumann rate flag
 					 [model-> 1-ile, 2-mohr] [Jaumn flag-> 0-no, 1-consider Jaumn rate] 
 
 line5->   if model=1 [ile]  1-Young's modulus, 2-Poison's ratio, 3-phi, 
           if model=2 [mohr] 1-Young's modulus, 2-Poison's ratio, 3-phi, 4-psi, 5-coh, 
                             6-sigt  
 
 line6->   [Mesh]   1-start ymin, 2-start ymax, 3-No. of meshes in y dirn
 
 line7(Mesh)    ->  1-Mesh id, 2-mesh type, 3-xmin, 4-xmax, 5-No. of meshes in x dir,
                    6-ymin, 7-ymax bot   [mesh type-> 1-rectangular, 2-slope]
 
 line9(Boundary)->  1-Boundary No., 2-mesh id, 3-boundary type, 4-location 5-miu                     
                    [boundary type-> 1-x or y dir, 2-slope]
                    [location-> 1-bot, 2-top, 3-LHS, 4-RHS ]
 

 [ data file-> 1-particle id, 2-x coordinate, 3-y coordinate	]

#--------------------------------------------------------------------#
data file: d_slope_0025_gran.txt
particle interval : 0.0025m
