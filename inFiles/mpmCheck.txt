1e-5	10	1
1	0.509
2	6 
2	0.2
2650	1e6	0.3	30.0	0	0	0	0.3
0.0	0.75	1 
0	0	0.0	1.0	1	0.0	0.0 
1	0	1.0	2.0	2	0.0	0.0 
0	0	1	0
1	0	3	0
2	0	1	1
3	1	1	0
4	1	4	0
5	1	1	1
________________________________________________________________
1	2	3	4	5	6	7	8

#--------------------------------------------------------------------# 
 line1->   1-dt, 2-Number of time steps, 3-writing steps
  
 line2->   1-gravity flag (0/1), 2-miu(boundary)
  
 line3->   1-No. of mesh zones, 2-No. of boudary zones  
  
 line4->   1-model  [model-> 1-ile, 2-mohr], 2-soil particle spacing**
  
 line5->   if model=1 [ile]
             1-density, 2-Young's modulus, 3-Poison's ratio, 4-phi, 5-porosity 
           if model=2 [mohr]
	     1-density, 2-Young's modulus, 3-Poison's ratio, 4-phi, 5-psi,
	                     6-coh, 7-sigt, 8-porosity   
  
 line6->   [Mesh]   1-start ymin, 2-start ymax, 3-No. of meshes in y dirn 
  
 line7(Mesh)    ->  1-Mesh id, 2-mesh type, 3-xmin, 4-xmax, 5-No. of meshes in x dir, 
                    6-ymin, 7-ymax bot   [mesh type-> 0-rectangular, 1-slope] 
  
 line9(Boundary)->  1-Boundary No, 2-mesh id, 3-location 4-boundary type(general/friction)
                    [location-> 1-bot, 2-top, 3-LHS, 4-RHS ] 
                    [boundary type-> 0-general(put v/a to zero), 1-friction ] 
  
 
 [ data file-> 1-particle id, 2-x coordinate, 3-y coordinate	] 
 
#--------------------------------------------------------------------# 
 
